﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTDCafeteria
{
    public class ApplicationServiceResponse
    {
        private readonly ApplicationServiceResult _result;
        public ApplicationServiceResult Result { get { return _result; } }

        public ApplicationServiceResponse(ApplicationServiceResult result) 
        {
            _result = result;
        }

        private static ApplicationServiceResponse _success = new ApplicationServiceResponse(ApplicationServiceResult.Success);
        public static ApplicationServiceResponse Success { get { return _success; } }
    }

    public class ApplicationServiceResponse<T>:ApplicationServiceResponse
    {
        private readonly T _payload;
        public T Payload { get { return _payload; } }

        public ApplicationServiceResponse(ApplicationServiceResult result, T payload)
            :base(result)
        {
            _payload = payload;
        }
    }

    public class ApplicationServiceListResponse<T>:ApplicationServiceResponse<IEnumerable<T>>
    {
        public ApplicationServiceListResponse(ApplicationServiceResult result, IEnumerable<T> payload)
                :base(result, payload)
        {
        }
    }
}
