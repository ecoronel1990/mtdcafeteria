﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MTDCafeteria
{
    public class CafeteriaWorker:IDisposable
    {
        private string codigoTarjeta = string.Empty;
        //OfflineWorker _offlineWorker;
        private string _LOCAL_CONFIG_FILE_PATH = AppDomain.CurrentDomain.BaseDirectory + "\\StaConfig.txt";
        private string _LOCAL_AXTRAX_FILE_PATH = AppDomain.CurrentDomain.BaseDirectory + "\\AxusHex.txt";
        private string _LOCAL_CAFCOME_FILE_PATH = AppDomain.CurrentDomain.BaseDirectory + "\\CafCome.txt";
        private static string _EMPLOYEE_FILE_PATH = AppDomain.CurrentDomain.BaseDirectory + "\\employeesTRESS.txt";
        List<AxtraxEmployee> axtraxEmployees = new List<AxtraxEmployee>();
        List<StationConfig> stationConfig = new List<StationConfig>();
        List<CafCome> cafeCome = new List<CafCome>();
        List<Employee> employees = new List<Employee>();

        public CafeteriaWorker()
        {
            //_offlineWorker = new OfflineWorker();
        }


        //private SqlConnection openTRESSConnection()
        //{
        //    string connectionString = "Data Source=NOGVWP-TRESS; Initial Catalog=TressPrueba; Persist Security Info = True; User ID=MTDnsql;Password=Nog@l!2";

        //    SqlConnection Connection = new SqlConnection(connectionString);
        //    //using (SqlConnection Connection = new SqlConnection(connectionString))
        //    //{
        //    //    try
        //    //    {
        //    //        Connection.Open();
        //    //        Connection.Close();
        //    //    }
        //    //    catch (Exception ex)
        //    //    {

        //    //        Console.WriteLine("Failure: {0}", ex.Message);
        //    //    }
        //        return Connection;
        //    //}
        //}

        public ApplicationServiceListResponse<StationConfig> GetStaConfig()
        {
            stationConfig = new List<StationConfig>();

            try
            {
                if (File.Exists(_LOCAL_CONFIG_FILE_PATH))
                {

                    using (StreamReader rw = new StreamReader(_LOCAL_CONFIG_FILE_PATH, true))
                    {
                        while (!rw.EndOfStream)
                        {
                            stationConfig.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<StationConfig>(rw.ReadLine()));
                        }
                    }
                }
                return new ApplicationServiceListResponse<StationConfig>(ApplicationServiceResult.Success, stationConfig);
            }
            catch (Exception ex)
            {
                return new ApplicationServiceListResponse<StationConfig>(ApplicationServiceResult.Exception, stationConfig);
            }
        }

        public ApplicationServiceListResponse<CafCome> GetCafCome()
        {
            cafeCome = new List<CafCome>();

            try
            {
                if (File.Exists(_LOCAL_CAFCOME_FILE_PATH))
                {

                    using (StreamReader rw = new StreamReader(_LOCAL_CAFCOME_FILE_PATH, true))
                    {
                        while (!rw.EndOfStream)
                        {
                            //cafeCome.Add(Newtonsoft.Json.JsonConvert.SerializeObject<CafCome>(rw.ReadLine()));
                            cafeCome.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<CafCome>(rw.ReadLine()));
                        }
                    }
                }
                return new ApplicationServiceListResponse<CafCome>(ApplicationServiceResult.Success, cafeCome);
            }
            catch (Exception ex)
            {
                return new ApplicationServiceListResponse<CafCome>(ApplicationServiceResult.Exception, cafeCome);
            }
        }

        public ApplicationServiceListResponse<AxtraxEmployee> GetAxtraxEmployees()
        {
            axtraxEmployees = new List<AxtraxEmployee>();

            try
            {
                if (File.Exists(_LOCAL_AXTRAX_FILE_PATH))
                {

                    using(StreamReader rw = new StreamReader(_LOCAL_AXTRAX_FILE_PATH, true))
                    {
                        while(!rw.EndOfStream)
                        {
                            axtraxEmployees.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<AxtraxEmployee>(rw.ReadLine()));
                        }
                    }
                }
                return new ApplicationServiceListResponse<AxtraxEmployee>(ApplicationServiceResult.Success, axtraxEmployees);
            }
            catch (Exception ex)
            {
               return new ApplicationServiceListResponse<AxtraxEmployee>(ApplicationServiceResult.Exception, axtraxEmployees);
            }
        }

        public string GetScheduleByHours(List<StationConfig> stationConfig)
        {
            var foundStation = stationConfig.FirstOrDefault(x => DateTime.Now.TimeOfDay >= x.InitTime && DateTime.Now.TimeOfDay <= x.EndTime);
            if (foundStation != null)
            {
                return foundStation.Tipo.ToString();                 
            }
      
            return string.Empty;
        }

        public string GetClockNumberByAxtraxCardCode(string hexCode, List<AxtraxEmployee> axtraxEmployees)
        {
            long cardCode = 0;
            int cardCode1 = ConvertHexToDecimal(hexCode.Substring(0, 4));
            int cardCode2 = ConvertHexToDecimal(hexCode.Substring(4, 4));
            cardCode = Convert.ToInt64((cardCode1.ToString() + cardCode2.ToString()));           

            string siteCode = hexCode;

            if(cardCode > 0)
            {
                var foundEmployee = axtraxEmployees.Where(x => x.CardId == cardCode).ToList().FirstOrDefault();
                if(foundEmployee != null)
                {
                    return foundEmployee.Number.ToString();
                }                
            }
            return string.Empty;
        }

        private string ConvertDecToHex(int DecNumber)
        {
            try
            {
                return DecNumber.ToString("X");
            }
            catch (Exception ex)
            {
                return "-1";
            }
        }
         
        private int ConvertHexToDecimal (string hexNumber)
        {
            try
            {
                return int.Parse(hexNumber, System.Globalization.NumberStyles.HexNumber);
            }
            catch(Exception ex)
            {
                return -1;
            }
        }

        //private DataSet QueryData(string sqlQuery)
        //{
        //    //SqlConnection Connection = openTRESSConnection();
        //    //Connection.Open();
        //    //SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlQuery, Connection);
        //    //DataSet data = new DataSet();
        //    //dataAdapter.Fill(data);
        //    //Connection.Close();
        //    //return data;


        //    using (SqlConnection Connection = openTRESSConnection())
        //    {
        //        try
        //        {
        //            Connection.Open();
        //            SqlDataAdapter dataAdapter = new SqlDataAdapter(sqlQuery, Connection);
        //            DataSet data = new DataSet();
        //            dataAdapter.Fill(data);
        //            Connection.Close();
        //            return data;
        //        }
        //        catch (Exception ex)
        //        {
        //            //Console.WriteLine("Failure: {0}", ex.Message);
        //            System.Windows.MessageBox.Show("Failure: {0}", ex.Message);
        //            DataSet data = new DataSet();
        //            //data = DBNull.Value;
        //            //data = dataSet.Clear();
        //            return data;
        //        }
        //        //return Connection;
        //    }
        //}

        private List<Employee> ConvertDataSetToEmployeeslist(DataSet dataSet)
        {
            DataTable data = dataSet.Tables[0];
            List<Employee> employeeList = new List<Employee>();
            foreach (DataRow r in data.Rows)
            {
                try
                {
                    employeeList.Add(new Employee()
                    {
                        Clocknumber = Convert.ToString(r[0]),
                        FullName = Convert.ToString(r[1]),
                        Image = (byte[])r["IM_BLOB"]

                    });
                    
                }
                catch (Exception)
                {
                    
                }
            }
            Task.Factory.StartNew(() =>
            {
                //_offlineWorker.SaveOfflineEmployees(employeeList);
            });

            return employeeList;
        }

        //public List<Employee> GetTRESSEmployees()
        //{
        //    return ConvertDataSetToEmployeeslist(QueryData("select i.CB_CODIGO, cc.PRETTYNAME, i.IM_BLOB from IMAGEN as i left join COLABORA as cc on i.CB_CODIGO=cc.CB_CODIGO where i.IM_TIPO='FOTO' and cc.CB_CODIGO=ANY(select CB_CODIGO from COLABORA as c where c.CB_ACTIVO='S');"));
        //}

        public ApplicationServiceListResponse<Employee> GetOfflineTRESSEmployees()
        {
            var results = new List<Employee>();
            try
            {
                if (File.Exists(_EMPLOYEE_FILE_PATH))
                {
                    using (StreamReader rw = new StreamReader(_EMPLOYEE_FILE_PATH, true))
                    {
                        while (!rw.EndOfStream)
                        {
                            results.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<Employee>(rw.ReadLine()));
                        }
                    }
                }
                return new ApplicationServiceListResponse<Employee>(ApplicationServiceResult.Exception, results);
            }
            catch (Exception)
            {
                return new ApplicationServiceListResponse<Employee>(ApplicationServiceResult.Exception, results);
            }

        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
