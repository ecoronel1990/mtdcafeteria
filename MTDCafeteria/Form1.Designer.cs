﻿namespace MTDCafeteria
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtBadgeId = new System.Windows.Forms.TextBox();
            this.lblEmployeeName = new System.Windows.Forms.Label();
            this.lblEmpNumber = new System.Windows.Forms.Label();
            this.lblProd = new System.Windows.Forms.Label();
            this.lblErrorMsg = new System.Windows.Forms.Label();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.lblFecha = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dtGridViewCafe = new System.Windows.Forms.DataGridView();
            this.lblEstacion = new System.Windows.Forms.Label();
            this.lblStation = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblHorario = new System.Windows.Forms.Label();
            this.lblSchedule = new System.Windows.Forms.Label();
            this.lblDBStatus = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCounter = new System.Windows.Forms.TextBox();
            this.btnTypeNumber = new System.Windows.Forms.PictureBox();
            this.btnPushed = new System.Windows.Forms.PictureBox();
            this.btnRefresh = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.btnConsumos = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureEmployee = new System.Windows.Forms.PictureBox();
            this.lblNumEmp = new System.Windows.Forms.Label();
            this.txtNumEmp = new System.Windows.Forms.TextBox();
            this.dtGridViewComenzales = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridViewCafe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTypeNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPushed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConsumos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEmployee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridViewComenzales)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBadgeId
            // 
            this.txtBadgeId.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBadgeId.Location = new System.Drawing.Point(728, 110);
            this.txtBadgeId.Name = "txtBadgeId";
            this.txtBadgeId.Size = new System.Drawing.Size(211, 39);
            this.txtBadgeId.TabIndex = 0;
            this.txtBadgeId.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBadgeId_KeyPress);
            // 
            // lblEmployeeName
            // 
            this.lblEmployeeName.AutoSize = true;
            this.lblEmployeeName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmployeeName.Location = new System.Drawing.Point(631, 344);
            this.lblEmployeeName.Name = "lblEmployeeName";
            this.lblEmployeeName.Size = new System.Drawing.Size(0, 32);
            this.lblEmployeeName.TabIndex = 2;
            // 
            // lblEmpNumber
            // 
            this.lblEmpNumber.AutoSize = true;
            this.lblEmpNumber.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEmpNumber.Location = new System.Drawing.Point(505, 344);
            this.lblEmpNumber.Name = "lblEmpNumber";
            this.lblEmpNumber.Size = new System.Drawing.Size(0, 32);
            this.lblEmpNumber.TabIndex = 3;
            // 
            // lblProd
            // 
            this.lblProd.AutoSize = true;
            this.lblProd.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblProd.Location = new System.Drawing.Point(1201, 612);
            this.lblProd.Name = "lblProd";
            this.lblProd.Size = new System.Drawing.Size(0, 32);
            this.lblProd.TabIndex = 4;
            // 
            // lblErrorMsg
            // 
            this.lblErrorMsg.AutoSize = true;
            this.lblErrorMsg.BackColor = System.Drawing.Color.LightGreen;
            this.lblErrorMsg.Font = new System.Drawing.Font("Arial Rounded MT Bold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorMsg.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblErrorMsg.Location = new System.Drawing.Point(504, 12);
            this.lblErrorMsg.MinimumSize = new System.Drawing.Size(935, 80);
            this.lblErrorMsg.Name = "lblErrorMsg";
            this.lblErrorMsg.Padding = new System.Windows.Forms.Padding(20, 10, 0, 0);
            this.lblErrorMsg.Size = new System.Drawing.Size(935, 80);
            this.lblErrorMsg.TabIndex = 6;
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(505, 113);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(217, 32);
            this.lblCodigo.TabIndex = 7;
            this.lblCodigo.Text = "Lectura Código";
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Font = new System.Drawing.Font("Arial Rounded MT Bold", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.Location = new System.Drawing.Point(1066, 529);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(112, 37);
            this.lblFecha.TabIndex = 8;
            this.lblFecha.Text = "Fecha";
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // dtGridViewCafe
            // 
            this.dtGridViewCafe.AllowUserToAddRows = false;
            this.dtGridViewCafe.AllowUserToDeleteRows = false;
            this.dtGridViewCafe.AllowUserToResizeColumns = false;
            this.dtGridViewCafe.AllowUserToResizeRows = false;
            this.dtGridViewCafe.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dtGridViewCafe.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGridViewCafe.Enabled = false;
            this.dtGridViewCafe.Location = new System.Drawing.Point(12, 529);
            this.dtGridViewCafe.MultiSelect = false;
            this.dtGridViewCafe.Name = "dtGridViewCafe";
            this.dtGridViewCafe.ReadOnly = true;
            this.dtGridViewCafe.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dtGridViewCafe.Size = new System.Drawing.Size(678, 260);
            this.dtGridViewCafe.TabIndex = 10;
            // 
            // lblEstacion
            // 
            this.lblEstacion.AutoSize = true;
            this.lblEstacion.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstacion.Location = new System.Drawing.Point(505, 426);
            this.lblEstacion.Name = "lblEstacion";
            this.lblEstacion.Size = new System.Drawing.Size(130, 32);
            this.lblEstacion.TabIndex = 11;
            this.lblEstacion.Text = "Terminal";
            // 
            // lblStation
            // 
            this.lblStation.AutoSize = true;
            this.lblStation.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStation.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lblStation.Location = new System.Drawing.Point(641, 426);
            this.lblStation.Name = "lblStation";
            this.lblStation.Size = new System.Drawing.Size(0, 32);
            this.lblStation.TabIndex = 12;
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.BackColor = System.Drawing.Color.Green;
            this.lblError.Font = new System.Drawing.Font("Arial Rounded MT Bold", 1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblError.Location = new System.Drawing.Point(12, 512);
            this.lblError.MinimumSize = new System.Drawing.Size(1425, 5);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(1425, 5);
            this.lblError.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(1068, 615);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 29);
            this.label2.TabIndex = 18;
            this.label2.Text = "Sirviendo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.label3.Location = new System.Drawing.Point(1068, 749);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(274, 28);
            this.label3.TabIndex = 20;
            this.label3.Text = "Consumos de Estacion";
            // 
            // lblHorario
            // 
            this.lblHorario.AutoSize = true;
            this.lblHorario.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHorario.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.lblHorario.Location = new System.Drawing.Point(1066, 675);
            this.lblHorario.Name = "lblHorario";
            this.lblHorario.Size = new System.Drawing.Size(101, 28);
            this.lblHorario.TabIndex = 21;
            this.lblHorario.Text = "Horario";
            // 
            // lblSchedule
            // 
            this.lblSchedule.AutoSize = true;
            this.lblSchedule.Font = new System.Drawing.Font("Arial Rounded MT Bold", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSchedule.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.lblSchedule.Location = new System.Drawing.Point(1178, 675);
            this.lblSchedule.Name = "lblSchedule";
            this.lblSchedule.Size = new System.Drawing.Size(0, 28);
            this.lblSchedule.TabIndex = 22;
            // 
            // lblDBStatus
            // 
            this.lblDBStatus.AutoSize = true;
            this.lblDBStatus.BackColor = System.Drawing.Color.Green;
            this.lblDBStatus.Font = new System.Drawing.Font("Arial Rounded MT Bold", 1F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDBStatus.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblDBStatus.Location = new System.Drawing.Point(11, 792);
            this.lblDBStatus.MinimumSize = new System.Drawing.Size(1425, 5);
            this.lblDBStatus.Name = "lblDBStatus";
            this.lblDBStatus.Size = new System.Drawing.Size(1425, 5);
            this.lblDBStatus.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(505, 301);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 32);
            this.label5.TabIndex = 24;
            this.label5.Text = "Numero";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(641, 301);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(306, 32);
            this.label6.TabIndex = 25;
            this.label6.Text = "Nombre del Empleado";
            // 
            // txtCounter
            // 
            this.txtCounter.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtCounter.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCounter.Enabled = false;
            this.txtCounter.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCounter.Location = new System.Drawing.Point(1346, 745);
            this.txtCounter.Name = "txtCounter";
            this.txtCounter.Size = new System.Drawing.Size(90, 32);
            this.txtCounter.TabIndex = 29;
            // 
            // btnTypeNumber
            // 
            this.btnTypeNumber.Image = global::MTDCafeteria.Properties.Resources.Typing3;
            this.btnTypeNumber.Location = new System.Drawing.Point(1089, 404);
            this.btnTypeNumber.Name = "btnTypeNumber";
            this.btnTypeNumber.Size = new System.Drawing.Size(112, 92);
            this.btnTypeNumber.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnTypeNumber.TabIndex = 30;
            this.btnTypeNumber.TabStop = false;
            this.btnTypeNumber.Click += new System.EventHandler(this.btnTypeNumber_Click);
            // 
            // btnPushed
            // 
            this.btnPushed.Image = global::MTDCafeteria.Properties.Resources.PushedButton;
            this.btnPushed.Location = new System.Drawing.Point(1317, 404);
            this.btnPushed.Name = "btnPushed";
            this.btnPushed.Size = new System.Drawing.Size(122, 92);
            this.btnPushed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnPushed.TabIndex = 28;
            this.btnPushed.TabStop = false;
            this.btnPushed.Visible = false;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Image = global::MTDCafeteria.Properties.Resources.RefreshButton1;
            this.btnRefresh.Location = new System.Drawing.Point(1317, 404);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(122, 92);
            this.btnRefresh.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnRefresh.TabIndex = 27;
            this.btnRefresh.TabStop = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            this.btnRefresh.MouseLeave += new System.EventHandler(this.btnRefresh_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::MTDCafeteria.Properties.Resources.Clock3;
            this.pictureBox5.Location = new System.Drawing.Point(972, 520);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(59, 62);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 19;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::MTDCafeteria.Properties.Resources.Calendar;
            this.pictureBox4.Location = new System.Drawing.Point(972, 665);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(59, 50);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 17;
            this.pictureBox4.TabStop = false;
            // 
            // btnConsumos
            // 
            this.btnConsumos.AccessibleDescription = "";
            this.btnConsumos.Image = global::MTDCafeteria.Properties.Resources.Counter;
            this.btnConsumos.Location = new System.Drawing.Point(972, 735);
            this.btnConsumos.Name = "btnConsumos";
            this.btnConsumos.Size = new System.Drawing.Size(59, 54);
            this.btnConsumos.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnConsumos.TabIndex = 16;
            this.btnConsumos.TabStop = false;
            this.btnConsumos.Click += new System.EventHandler(this.btnConsumos_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::MTDCafeteria.Properties.Resources.FoodPlate;
            this.pictureBox2.Location = new System.Drawing.Point(972, 600);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(69, 53);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 15;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::MTDCafeteria.Properties.Resources.MTDLogo;
            this.pictureBox1.Location = new System.Drawing.Point(1089, 106);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(350, 144);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // pictureEmployee
            // 
            this.pictureEmployee.Location = new System.Drawing.Point(12, 12);
            this.pictureEmployee.Name = "pictureEmployee";
            this.pictureEmployee.Size = new System.Drawing.Size(421, 484);
            this.pictureEmployee.TabIndex = 1;
            this.pictureEmployee.TabStop = false;
            // 
            // lblNumEmp
            // 
            this.lblNumEmp.AutoSize = true;
            this.lblNumEmp.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumEmp.Location = new System.Drawing.Point(505, 194);
            this.lblNumEmp.Name = "lblNumEmp";
            this.lblNumEmp.Size = new System.Drawing.Size(227, 32);
            this.lblNumEmp.TabIndex = 32;
            this.lblNumEmp.Text = "Teclear Numero";
            this.lblNumEmp.Visible = false;
            // 
            // txtNumEmp
            // 
            this.txtNumEmp.Font = new System.Drawing.Font("Arial Rounded MT Bold", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumEmp.Location = new System.Drawing.Point(738, 191);
            this.txtNumEmp.Name = "txtNumEmp";
            this.txtNumEmp.Size = new System.Drawing.Size(201, 39);
            this.txtNumEmp.TabIndex = 31;
            this.txtNumEmp.Visible = false;
            this.txtNumEmp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNumEmp_KeyDown);
            // 
            // dtGridViewComenzales
            // 
            this.dtGridViewComenzales.AllowUserToAddRows = false;
            this.dtGridViewComenzales.AllowUserToDeleteRows = false;
            this.dtGridViewComenzales.AllowUserToResizeColumns = false;
            this.dtGridViewComenzales.AllowUserToResizeRows = false;
            this.dtGridViewComenzales.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dtGridViewComenzales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGridViewComenzales.Enabled = false;
            this.dtGridViewComenzales.Location = new System.Drawing.Point(267, 640);
            this.dtGridViewComenzales.MultiSelect = false;
            this.dtGridViewComenzales.Name = "dtGridViewComenzales";
            this.dtGridViewComenzales.ReadOnly = true;
            this.dtGridViewComenzales.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dtGridViewComenzales.Size = new System.Drawing.Size(634, 149);
            this.dtGridViewComenzales.TabIndex = 33;
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(1454, 828);
            this.Controls.Add(this.dtGridViewComenzales);
            this.Controls.Add(this.lblNumEmp);
            this.Controls.Add(this.txtNumEmp);
            this.Controls.Add(this.btnTypeNumber);
            this.Controls.Add(this.txtCounter);
            this.Controls.Add(this.btnPushed);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblDBStatus);
            this.Controls.Add(this.lblSchedule);
            this.Controls.Add(this.lblHorario);
            this.Controls.Add(this.lblProd);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.btnConsumos);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblStation);
            this.Controls.Add(this.lblEstacion);
            this.Controls.Add(this.dtGridViewCafe);
            this.Controls.Add(this.lblFecha);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.lblErrorMsg);
            this.Controls.Add(this.lblEmpNumber);
            this.Controls.Add(this.lblEmployeeName);
            this.Controls.Add(this.pictureEmployee);
            this.Controls.Add(this.txtBadgeId);
            this.Name = "Form1";
            this.Text = "Sistema de Cafeteria";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtGridViewCafe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTypeNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPushed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnRefresh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConsumos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEmployee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridViewComenzales)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBadgeId;
        private System.Windows.Forms.PictureBox pictureEmployee;
        private System.Windows.Forms.Label lblEmployeeName;
        private System.Windows.Forms.Label lblEmpNumber;
        private System.Windows.Forms.Label lblProd;
        private System.Windows.Forms.Label lblErrorMsg;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridView dtGridViewCafe;
        private System.Windows.Forms.Label lblEstacion;
        private System.Windows.Forms.Label lblStation;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox btnConsumos;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblHorario;
        private System.Windows.Forms.Label lblSchedule;
        private System.Windows.Forms.Label lblDBStatus;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox btnRefresh;
        private System.Windows.Forms.PictureBox btnPushed;
        private System.Windows.Forms.TextBox txtCounter;
        private System.Windows.Forms.PictureBox btnTypeNumber;
        private System.Windows.Forms.Label lblNumEmp;
        private System.Windows.Forms.TextBox txtNumEmp;
        private System.Windows.Forms.DataGridView dtGridViewComenzales;
    }
}

