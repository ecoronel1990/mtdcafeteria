﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTDCafeteria
{
    public class StationConfig
    {
        public string StcName { get; set; }
        public string Tipo { get; set; }
        public string Nombre { get; set; }
        public string HoraIni { get; set; }
        public string HoraFin { get; set; }
        public TimeSpan InitTime
        {
            get { return new TimeSpan(Convert.ToInt32(HoraIni.Split(':')[0]), Convert.ToInt32(HoraIni.Split(':')[1]), 0); }
        }
        public TimeSpan EndTime
        {
            get { return new TimeSpan(Convert.ToInt32(HoraFin.Split(':')[0]), Convert.ToInt32(HoraFin.Split(':')[1]), 0); }
        }
    }
}
