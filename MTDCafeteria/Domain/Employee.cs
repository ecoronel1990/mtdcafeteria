﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTDCafeteria
{
    public class Employee
    {
        public string Clocknumber { get; set; }
        public string FullName { get; set; }
        public byte[] Image { get; set; }
    }
}
