﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTDCafeteria
{
    public class AxtraxEmployee
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public int SiteCode { get; set; }
        public int FiveDigits { get; set; }
        public int CardId { get; set; }
        public int CardStatus { get; set; }
        public string CardDesc { get; set; }
    }
}
