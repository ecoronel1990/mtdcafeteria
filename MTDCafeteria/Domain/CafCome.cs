﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTDCafeteria
{
    public class CafCome
    {
        public string EmpNumber { get; set; }
        public string ServiceType { get; set; }
        public string CCDate { get; set; }
        public string CCHour { get; set; }
        public string CCStation { get; set; }
    }
}
