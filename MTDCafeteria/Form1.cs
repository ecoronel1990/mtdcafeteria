﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MTDCafeteria
{
    public partial class Form1 : Form
    {
        private string _LOCAL_CAFCOME_FILE_PATH = AppDomain.CurrentDomain.BaseDirectory + "\\CafCome.txt";
        private string _LOCAL_CONTADOR_FILE_PATH = AppDomain.CurrentDomain.BaseDirectory + "\\Contador.txt";
        private string _badgeCode;
        private List<Employee> _employeesList;
        private CafeteriaWorker _cafeteriaWorker;
        private Employee _selectedEmployee;
        private List<AxtraxEmployee> axtraxEmployees;
        private List<StationConfig> _stationList;
        private List<CafCome> _cafeComeList;
        private StationConfig _selectedStation;

        private SqlConnection openTRESSConnection()
        {
            try
            {
                string connectionString = "Data Source=NOGVWP-TRESS; Initial Catalog=TressPrueba; Persist Security Info = True; User ID=MTDnsql;Password=Nog@l!2";

                SqlConnection Connection = new SqlConnection(connectionString);

                return Connection;
            }
            catch (Exception ex)
            {
                lblDBStatus.BackColor = System.Drawing.Color.Red;

                return null;
                //MessageBox.Show(this, "Db Not available: " + ex.Message);
            }

        }

        //private SqlConnection openConfigConn()
        //{

        //    string connectionString = "Server=localhost;Database=Config;Integrated Security=true";

        //    SqlConnection ConnConfig = new SqlConnection(connectionString);

        //    return ConnConfig;            
        //}

        public Form1()
        {
            InitializeComponent();
            pictureEmployee.BackgroundImageLayout = ImageLayout.Stretch;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //this.WindowState = FormWindowState.Maximized;


            lblEmpNumber.Text = "";
            lblEmployeeName.Text = "";   
            pictureEmployee.Image = null;
            timer1.Start();

            initCounter();

            //txtBadgeId.Cursor
            
            InitComponents();
        }

        private void InitComponents()
        {
            _badgeCode = string.Empty;
            _cafeteriaWorker = new CafeteriaWorker();
            _employeesList = new List<Employee>();
            _stationList = new List<StationConfig>();
            _cafeComeList = new List<CafCome>();
            //_employeesList = _cafeteriaWorker.GetTRESSEmployees();
            _employeesList = _cafeteriaWorker.GetOfflineTRESSEmployees().Payload.ToList(); 
            axtraxEmployees = _cafeteriaWorker.GetAxtraxEmployees().Payload.ToList();
            _stationList = _cafeteriaWorker.GetStaConfig().Payload.ToList();
            _cafeComeList = _cafeteriaWorker.GetCafCome().Payload.ToList();

        }

        private void initCounter()
        {
            string files = System.IO.Path.Combine(@_LOCAL_CONTADOR_FILE_PATH);

            if (!System.IO.File.Exists(files))
            {
                txtCounter.Text = "0";
            }
            else
            {
                TextReader txtReader = new StreamReader(@_LOCAL_CONTADOR_FILE_PATH);
                string rline = txtReader.ReadLine();
                if (rline != null)
                {
                    txtCounter.Text = rline;
                }
                else
                {
                    txtCounter.Text = "0";
                }
                txtReader.Close();
            }  
        }

        private Employee FindbyClockNumber(string clockNumber)
        {
            return _employeesList.Where(x => x.Clocknumber.Trim().Equals(clockNumber, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
        }

        private StationConfig FindByType(string stationType)
        {
            return _stationList.Where(x => x.Tipo.Trim().Equals(stationType, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
        }

        private Bitmap ConvertByteArrayToBitmap(byte[] bytes)
        {
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                return new Bitmap(ms);
            }
        }

        public DataTable CafeInfo()
        {
            
            try
            {
                SqlConnection Connection = openTRESSConnection();

                Connection.Open();

                DateTime datePrev = DateTime.Now;
                datePrev = datePrev.AddDays(-1);
                DateTime dateNxt = DateTime.Now;
                dateNxt = dateNxt.AddDays(1);
                SqlDataAdapter cafeInfo = new SqlDataAdapter("SELECT NUMERO, FECHA, HORA, PRODUCTO, ESTACION FROM [V_CAF_COME] WHERE FECHA > '" + datePrev + "' AND FECHA < '" + dateNxt + "' AND NUMERO = " + _selectedEmployee.Clocknumber, Connection);

                DataTable dt = new DataTable();
                cafeInfo.Fill(dt);

                foreach (DataRow row in dt.Rows)
                {

                    var rowValue = row["PRODUCTO"].ToString();
                        

                    var productStation = _stationList.FirstOrDefault(x => x.Tipo == rowValue);

                    if (productStation != null)
                    {
                        rowValue = productStation.Nombre.ToString();
                    }
                    else
                    {
                        rowValue = "";
                    }

                    row.SetField("PRODUCTO", rowValue);

                }

                Connection.Close();

                return dt;
            }
            catch (Exception ex)
            {
                lblDBStatus.BackColor = System.Drawing.Color.Red;

                return null;
                //MessageBox.Show(this, "Db Not available: " + ex.Message);
            }

        }

        //public DataTable ConfigInfo()
        //{
        //    SqlConnection ConnConfig = openConfigConn();

        //    ConnConfig.Open();

        //    SqlDataAdapter ConfigInfo = new SqlDataAdapter("SELECT * FROM SCHEDULES", ConnConfig);

        //    ConnConfig.Close();

        //    DataTable dt = new DataTable();
        //    ConfigInfo.Fill(dt);

        //    return dt;
        //}

        //public static DataTable ConvertCafeListToDataTable(List<string[]> cafeList)
        //{
        //    // New table.
        //    DataTable cafeTable = new DataTable();

        //    // Get max columns.
        //    int cafeColumns = 0;
        //    foreach (var array in cafeList)
        //    {
        //        if (array.Length > cafeColumns)
        //        {
        //            cafeColumns = array.Length;
        //        }
        //    }

        //    // Add columns.
        //    for (int i = 0; i < cafeColumns; i++)
        //    {
        //        cafeTable.Columns.Add();
        //    }

        //    // Add rows.
        //    foreach (var array in cafeList)
        //    {
        //        cafeTable.Rows.Add(array);
        //    }

        //    return cafeTable;
        //}

        private void ScreenData(string clockNumber)
        {
            var stationConfigs = _cafeteriaWorker.GetStaConfig();
            var cafecomefile = _cafeteriaWorker.GetCafCome();
            var stationType = _cafeteriaWorker.GetScheduleByHours(stationConfigs.Payload.ToList());

            _selectedEmployee = FindbyClockNumber(clockNumber.Trim());
            _selectedStation = FindByType(stationType.Trim());

            if (_selectedStation != null)
            {
                if (_selectedEmployee != null)
                {
                    txtBadgeId.Text = "";
                    lblEmployeeName.Text = _selectedEmployee.FullName;
                    lblEmpNumber.Text = _selectedEmployee.Clocknumber;
                    lblProd.Text = _selectedStation.Nombre;
                    lblSchedule.Text = _selectedStation.HoraIni + " a " + _selectedStation.HoraFin;
                    lblStation.Text = _selectedStation.StcName;
                    pictureEmployee.Image = ConvertByteArrayToBitmap(_selectedEmployee.Image);
                    pictureEmployee.SizeMode = PictureBoxSizeMode.StretchImage;
                    lblErrorMsg.Text = "Buen Provecho....!";
                    lblErrorMsg.BackColor = System.Drawing.Color.LightGreen;
                    lblError.BackColor = System.Drawing.Color.LightGreen;

                    var strCom = string.Empty;
                    var strSQL = string.Empty;
                    try
                    {

                        SqlConnection Connection = openTRESSConnection();

                        Connection.Open();
                        strCom = "SELECT ISNULL(CB_CODIGO, '') FROM CAF_COME WHERE CB_CODIGO = " + _selectedEmployee.Clocknumber + " AND CF_FECHA = '" + DateTime.Now.ToShortDateString() + "' AND CF_TIPO = " + _selectedStation.Tipo;
                        strSQL = "INSERT INTO CAF_COME ([CB_CODIGO],[CF_FECHA],[CF_HORA],[CF_TIPO],[CF_COMIDAS],[CF_EXTRAS],[CF_RELOJ],[US_CODIGO],[CL_CODIGO],[CF_REG_EXT],[CF_OFFLINE]) VALUES (" + _selectedEmployee.Clocknumber + ",'" + DateTime.Now.ToShortDateString() + "','" + DateTime.Now.ToString("hhmm") + "'," + _selectedStation.Tipo + ",1,0,'" + _selectedStation.StcName + "',0,0,'N','N')";

                        SqlCommand cmdC = new SqlCommand(strCom, Connection);
                        cmdC.ExecuteNonQuery();

                        var numEmp = cmdC.ExecuteScalar();
                        
                        ////////////////////////////////////////////////////////////////////  
                        //List<CafCome> cafeList = new List<CafCome>();


                        //if (File.Exists(_LOCAL_CAFCOME_FILE_PATH))
                        //{

                        //    using (StreamReader rw = new StreamReader(_LOCAL_CAFCOME_FILE_PATH, true))
                        //    {
                        //        while (!rw.EndOfStream)
                        //        {
                        //            _cafeComeList.Add(Newtonsoft.Json.JsonConvert.DeserializeObject<CafCome>(rw.ReadLine()));
                        //        }
                        //    }
                        //}


                        dtGridViewComenzales.DataSource = _cafeComeList; 
                        
                        if (numEmp == null)
                        {
                            var cmd = new SqlCommand(strSQL, Connection);

                            //JSON File **** CafCome.txt ****
                            List<CafCome> _cafcome = new List<CafCome>();
                            _cafcome.Add(new CafCome()
                            {
                                EmpNumber = _selectedEmployee.Clocknumber,
                                CCDate = DateTime.Now.ToShortDateString(),
                                ServiceType = _selectedStation.Tipo,
                                CCHour = DateTime.Now.ToString("hhmm"),
                                CCStation = _selectedStation.StcName
                            });

                            
                            string cafcomefile = JsonConvert.SerializeObject(_cafcome, Formatting.None);
                            File.AppendAllText(@_LOCAL_CAFCOME_FILE_PATH, cafcomefile);

                            int counterInt = Convert.ToInt32(txtCounter.Text);
                            counterInt = counterInt + 1;
                            txtCounter.Text = counterInt.ToString();
                            //JSON File **** Contador.txt ****
                            TextWriter txt = new StreamWriter(@_LOCAL_CONTADOR_FILE_PATH);
                            txt.Write(txtCounter.Text);
                            txt.Close();

                            cmd.ExecuteNonQuery();
                            dtGridViewCafe.DataSource = CafeInfo();
                            Connection.Close();
                        }
                        else
                        {
                            dtGridViewCafe.DataSource = CafeInfo();
                            Connection.Close();
                            lblErrorMsg.Text = "DUPLICADO: YA RECIBIO EL SERVICIO DE " + lblProd.Text.ToUpper(new CultureInfo("en-US", false));
                            lblErrorMsg.BackColor = System.Drawing.Color.Red;
                            lblError.BackColor = System.Drawing.Color.Red;
                        }
                    }

                    catch (Exception ex)
                    {
                        dtGridViewCafe.DataSource = null;
                        
                        //JSON File **** CafCome.txt ****
                        List<CafCome> _cafcome = new List<CafCome>();
                        _cafcome.Add(new CafCome()
                        {
                            EmpNumber = _selectedEmployee.Clocknumber,
                            CCDate = DateTime.Now.ToShortDateString(),
                            ServiceType = _selectedStation.Tipo,
                            CCHour = DateTime.Now.ToString("hhmm"),
                            CCStation = _selectedStation.StcName
                        });

                        string cafcomefile = JsonConvert.SerializeObject(_cafcome, Formatting.None);
                        File.AppendAllText(@_LOCAL_CAFCOME_FILE_PATH, cafcomefile);


                        int counterInt = Convert.ToInt32(txtCounter.Text);
                        counterInt = counterInt + 1;
                        txtCounter.Text = counterInt.ToString();
                        //JSON File **** Contador.txt ****
                        TextWriter txt = new StreamWriter(@_LOCAL_CONTADOR_FILE_PATH);
                        txt.Write(txtCounter.Text);
                        txt.Close();

                        lblDBStatus.BackColor = System.Drawing.Color.Red;

                        //MessageBox.Show(this, "Db Not available: " + ex.Message);
                    }
                    
                }
                else
                {
                    txtBadgeId.Text = "";
                    lblErrorMsg.Text = "EMPLEADO NO ENCONTRADO CON ESE CODIGO";
                    lblErrorMsg.BackColor = System.Drawing.Color.Orange;
                    lblError.BackColor = System.Drawing.Color.Orange;
                    lblEmpNumber.Text = "";
                    lblEmployeeName.Text = "";
                    pictureEmployee.Image = null;
                    dtGridViewCafe.DataSource = null;
                }
            }
            else
            {
                txtBadgeId.Text = "";
                lblErrorMsg.Text = "FUERA DE HORARIO, NO SE REGISTRARA";
                lblErrorMsg.BackColor = System.Drawing.Color.OrangeRed;
                lblError.BackColor = System.Drawing.Color.OrangeRed;
                lblEmpNumber.Text = "";
                lblEmployeeName.Text = "";
                lblProd.Text = "";
                lblStation.Text = "";
                pictureEmployee.Image = null;
                dtGridViewCafe.DataSource = null;
            }
        }

        private void txtBadgeId_KeyPress(object sender, KeyPressEventArgs e)
        {
            lblDBStatus.BackColor = System.Drawing.Color.Green;
            lblErrorMsg.Text = "";

            if (e.KeyChar == Convert.ToChar(Keys.Enter))
            {
                _badgeCode = txtBadgeId.Text;
                if (!string.IsNullOrEmpty(_badgeCode))
                {
                    string badgeSubstring = string.Empty;
                    try
                    {
                        if ((_badgeCode.Length - 8) > 0 && (_badgeCode.Length - 8) < 11)                        
                        {
                            badgeSubstring = _badgeCode.Substring(_badgeCode.Length - 8);
                        }
                        else
                        {
                            badgeSubstring = "0E00000000";
                        }
                    }
                    catch
                    {

                    }

                    var clockNumber = "";
                    if (_badgeCode.Length > 0 && _badgeCode.Length < 11)
                    {
                        clockNumber = _cafeteriaWorker.GetClockNumberByAxtraxCardCode(badgeSubstring.ToString(), axtraxEmployees);
                    }

                    ScreenData(clockNumber);
                    
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime dateTime = DateTime.Now;
            this.lblFecha.Text = dateTime.ToString();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            btnPushed.Visible = true;
            initCounter();
            this.Refresh();
            btnPushed.Visible = false;

        }

        private void btnConsumos_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Inicializara a Cero el Contador" + "\n" + "Estas Seguro?", "Confirmacion", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                //MessageBox.Show("Seleccionaste Si");
                txtCounter.Text = "0";
                //JSON File **** Contador.txt ****
                TextWriter txt = new StreamWriter(@_LOCAL_CONTADOR_FILE_PATH);
                txt.Write(txtCounter.Text);
                txt.Close();
            }
        }

        private void btnTypeNumber_Click(object sender, EventArgs e)
        {
            lblCodigo.Visible = false;
            txtBadgeId.Visible = false;
            lblNumEmp.Visible = true;
            txtNumEmp.Visible = true;
            txtNumEmp.Focus();

        }

        private void txtNumEmp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var clockNumber = txtNumEmp.Text;
                ScreenData(clockNumber);
                txtNumEmp.Clear();
                lblCodigo.Visible = true;
                txtBadgeId.Visible = true;
                lblNumEmp.Visible = false;
                txtNumEmp.Visible = false;
            }
        }

    }
}
